#!/bin/bash

infile=$1
outfile=$2
fasta_ref=$3   # must be hg19

basepath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

positions=$basepath/data/ESP6500SI-V2-SSA137_XY_hg19.bed

samtools mpileup \
	--fasta-ref $fasta_ref \
	--positions $positions \
	--BCF --uncompressed \
	$infile |
	bcftools call \
		--multiallelic-caller \
		--output-type z \
		--output $outfile
