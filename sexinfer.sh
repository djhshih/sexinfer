#!/bin/bash

inbam=$1
outfile=$2
fasta_ref=$3   # must be hg19

bindir=.
outdir=.

fbase=${inbam##*/}
fstem=${fstem%.*}

mkdir -p xy-vcf xy-tsv

$bindir/call-xy-snps.sh $inbam xy-vcf/${fstem}_xy.vcf.gz $fasta_ref
$bindir/parse_vcf.py ${fstem}_xy.vcf.gz xy-tsv/${fstem}_xy.tsv

# after running all samples
# then you must call sex by clustering manually using gather.R and analyze.R,
# after updating the scripts to use appropriate files
# sexinfer.R is not ready
