library(io);
library(dplyr);

fnames <- list_files("xy-tsv", pattern=".*_xy\\.tsv");
fnames.full <- list_files("xy-tsv", pattern=".*_xy\\.tsv", full.names=TRUE);
samples <- sub("_xy\\.tsv", "", fnames);

xs <- lapply(
	fnames.full,
	function(f) {
		x <- qread(f);
		mutate(x,
			dp_ref = dp_ref_f + dp_ref_r,
			dp_alt = dp_alt_f + dp_alt_r,
			dp = dp_ref + dp_alt
		)
	}
);
names(xs) <- samples;

qwrite(xs, "genotypes_dp4-x-y.rds");
