#/!bin/bash

infile=$1   # list of bam files
indir=xy-vcf
outdir=xy-tsv
fvcf_suffix=_xy.vcf.gz
ftsv_suffix=_xy.tsv

basepath="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

mkdir -p $outdir

for fpath in $(cat $infile); do
	fbase=${fpath##*/}
	fstem=${fbase%.*}
	fvcf=${indir}/${fstem}${fvcf_suffix}
	ftsv=${outdir}/${fstem}${ftsv_suffix}
	if [[ ! -f $ftsv ]]; then
		echo "$ftsv"
		${basepath}/parse_vcf.py $fvcf $ftsv
	fi
done

