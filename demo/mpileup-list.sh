#/!bin/bash

infile=$1    # list of bam files 
fsuffix=_xy.vcf.gz
outdir=$(pwd)/xy-vcf

fasta_ref=$2

basepath="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

mkdir -p $outdir
mkdir -p jobs

for fpath in $(cat $infile); do
	fbase=${fpath##*/}
	fstem=${fbase%.*}
	outfile=${outdir}/${fstem}${fsuffix}
	if [[ ! -f $outfile ]]; then
		echo $fpath
		echo "$basepath/call-xy-snps.sh $fpath $outfile $fasta_ref" > jobs/mpileup_${fstem}.sh
		bash jobs/mpileup_${fstem}.sh
		#qsub -V jobs/mpileup_${fstem}.sh
	fi
done
