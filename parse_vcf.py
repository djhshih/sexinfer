#!/usr/bin/env python3

# Date: 2016-04-26
# Author: David J. H. Shih <djh.shih@gmail.com>
# License: GPLv3


import argparse
import gzip
import sys
from pyparsing import Word, Literal, Optional, alphas, alphanums, nums, removeQuotes, quotedString, Suppress, OneOrMore, restOfLine, Group, Combine, Dict, delimitedList, ParseException

pr = argparse.ArgumentParser("Parse VCFfile for read depths")
pr.add_argument("input", help="input VCF file")
pr.add_argument("output", help="output file")
pr.add_argument("--delim", help="output delimiter", default='\t')

argv = pr.parse_args()

## Grammar for VCF format

cvtInt = lambda s, l, toks: int(toks[0])
cvtReal = lambda s, l, toks: float(toks[0])

comment = Suppress("#" + Optional("#")) + restOfLine
empty = Literal(".")
nan = Literal("nan") | Literal("NaN")

chrom = Word(alphanums)("chrom")
pos = Word(nums)("pos")
id_list = empty | Word(alphanums)("id")
ref_base = empty | Word(alphas)("ref")
alt_base = empty | Word(alphas + ",")("alt")
quality = empty | Word(nums + ".")("qual") | nan
filter_status = empty | Word(alphas)("filter")
attribute_value = Word(alphanums + 'e-.')
attribute_values = empty | delimitedList(attribute_value, ",") | attribute_value
attribute = Word(alphanums) + Optional(Suppress("=") + attribute_values)
info = Dict(delimitedList(Group(attribute), ";"))("info")
genotype_format = Word(alphanums + ":")
sample_genotype = Word(alphanums + "/:,.")

variant = chrom + pos + id_list + ref_base + alt_base + quality + filter_status + info + genotype_format + sample_genotype


# open output file

header = ["chromosome", "position", "ref", "alt", "dp_ref_f", "dp_ref_r", "dp_alt_f", "dp_alt_r"]
delim = argv.delim

outf = open(argv.output, 'w')
outf.write(delim.join(header) + '\n')

# process input file

#with open(argv.input) as inf:
with gzip.open(argv.input, 'rt') as inf:
    for line in inf:
        try:
            comment.parseString(line)
        except:
            try:
                g = variant.parseString(line)
                if not "INDEL" in g.info:
                    xs = [g.chrom, g.pos, g.ref, g.alt] + [x for x in g.info.DP4]
                    outf.write( delim.join(xs) + '\n' )
            except ParseException as e:
                sys.stderr.write("Cannot parse:\n" + line + '\n')
                sys.stderr.write(str(e))

outf.close()

